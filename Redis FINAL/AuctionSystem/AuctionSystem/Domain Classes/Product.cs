﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ServiceStack.Redis;
using ServiceStack.Text;

namespace AuctionSystem.Domain_Classes
{
    public class Product
    {
        public User user;
        public string Name;
        public string Description;
        public float StartPrise;
        public float BidPrise;
        public DateTime BidEnd;

        public void FinishBidding (RedisClient client,string buyer)
        {
            string allProductsId = "allProducts";
            string hashId = user.Username + ":" + Name;
            string userProductsId = user.Username + ":products";
            string bidKey = user.Username + ":" + Name + ":bids";

            RemoveProductBids(client);
            client.RemoveItemFromList(allProductsId, user.Username + "." + Name);
            client.Remove(hashId);
            client.RemoveItemFromList(userProductsId, Name);
            client.Remove(bidKey);

            if (buyer != null)
            {
                string boughtProductKey = buyer + ":boughtProducts";
                client.RPush(boughtProductKey, Encoding.UTF8.GetBytes(user.Username + ":" + Name));
            }
            client.Save();
        }
        public Product (string name,string description,float startPrice,DateTime bidEnd)
        {
            Name = name;
            Description = description;
            StartPrise = startPrice;
            BidEnd = bidEnd;
            BidPrise = StartPrise;
        }
        public Product(string name, string description, float startPrice,float bidPrice, DateTime bidEnd)
        {
            Name = name;
            Description = description;
            StartPrise = startPrice;
            BidEnd = bidEnd;
            BidPrise = bidPrice;
        }
        public Product (String name)
        {
            Name = name;
        }
        public static Product GetProduct (RedisClient client,string owner,string productName)
        {
            string productKey = owner + ":" + productName; //pravimo kljuc
            byte[][] prod = client.HMGet(productKey, new byte[][]{
                Encoding.UTF8.GetBytes ("Description"),
                Encoding.UTF8.GetBytes ("StartPrice"),
                Encoding.UTF8.GetBytes ("BidPrice"),
                Encoding.UTF8.GetBytes ("ExpDate"),
                Encoding.UTF8.GetBytes("Owner")});//ucitamo produkt

            if (prod[0] != null)
            {
                Product p;
                float startPrice = Single.Parse(Encoding.UTF8.GetString(prod[1]));//uzimam atribut koji mi treba
                float bidPrice = Single.Parse(Encoding.UTF8.GetString(prod[2]));
                DateTime date = DateTime.Parse(Encoding.UTF8.GetString(prod[3]));
                string ownerUsername = Encoding.UTF8.GetString(prod[4]);
                p = new Product(productName, Encoding.UTF8.GetString(prod[0]), startPrice, bidPrice, date);
                p.user = new User (ownerUsername);
                return p;
            }
            else
                return null;
        }
        public void RemoveProductBids (RedisClient client)
        {
            string bidListKey = user.Username + ":" + Name + ":bids";
            byte[][] bidList = client.LRange(bidListKey, 0, -1);

            int count = bidList.Length;
            for (int i = 0; i < count; i++)
            {
                string bid = Encoding.UTF8.GetString(bidList[i]);
                string[] bidSplited = bid.Split(':');
                string item = user.Username + ":" + Name + ":" + bidSplited[1];
                string key = bidSplited[0] + ":myBids";
                client.RemoveItemFromList(key, item);
            }
            client.Save();
        }
        
    }
}
