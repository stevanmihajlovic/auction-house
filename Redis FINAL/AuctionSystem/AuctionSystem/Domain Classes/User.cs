﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ServiceStack.Redis;
using ServiceStack.Text;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace AuctionSystem.Domain_Classes
{
    public class User
    {
       public List<Product> products;
       public List<Product> productBids;
       public string Username;
       public string Password;
       public string Name;
       public string Lastname;
       public float Money;
       public int PositiveVotes;
       public int NegativeVotes;
        

        public bool AddProduct (Product p,RedisClient client)
        {
            string hashid = Username + ":" + p.Name;
            string productlist = Username+":"+"products";
            var product = client.HGet(hashid, Encoding.UTF8.GetBytes("Name"));

            if (product == null)
            {

                byte[][] keys = 
            {
                Encoding.UTF8.GetBytes ("Name"),
                Encoding.UTF8.GetBytes ("Description"),
                Encoding.UTF8.GetBytes ("StartPrice"),
                Encoding.UTF8.GetBytes ("BidPrice"),
                Encoding.UTF8.GetBytes ("ExpDate"),
                Encoding.UTF8.GetBytes ("Owner")
            };
                byte[][] values = 
            {
                Encoding.UTF8.GetBytes (p.Name),
                Encoding.UTF8.GetBytes (p.Description),
                Encoding.UTF8.GetBytes (p.StartPrise.ToString ()),
                Encoding.UTF8.GetBytes (p.BidPrise.ToString ()),
                Encoding.UTF8.GetBytes (p.BidEnd.ToString ()),
                Encoding.UTF8.GetBytes (Username)
            };

                p.user = this;
                products.Add(p);

                client.HMSet(hashid, keys, values);
                client.RPush(productlist, Encoding.UTF8.GetBytes(p.Name));           
                string pList = "allProducts";
                client.RPush(pList, Encoding.UTF8.GetBytes(Username+"."+p.Name));
                return true;
            }
            else
            {
                return false;
            }
            client.Save();
        }
        public void BidProduct (Product p,RedisClient client,float value)
        {
                //lista bidova za konkretni produkt
                string bidKey =p.user.Username + ":" + p.Name  + ":bids";
               
                p.BidPrise = value;

              // byte[][] lastBid = client.LRange(bidKey, -1, -1);
               string lastBid = client.GetItemFromList(bidKey, -1);
               if (lastBid != null)
               {
                   //string bid = Encoding.UTF8.GetString(lastBid[0]);
                   string[] userValue = lastBid.Split(':');
                   User u = User.GetUser(client, userValue[0]);
                   float price = Single.Parse(userValue[1]);
                   if (u.Username != Username)//nije nasa ponuda najbolja
                   {
                       u.Money += price;
                       u.SaveUser(client);
                       Money -= price;
                   }
                   else//ako bidujemo ponovo, a nasa ponuda je trenutno najbolja
                   {
                       Money -= (value - price);//umanjimo samo za razliku
                       SaveUser(client);
                   }
               }
               else
               {
                   Money -= value;
               }
               //dodamo novi bid
               client.RPush(bidKey, Encoding.UTF8.GetBytes(Username + ":" + value.ToString()));

               //update trenutne cene proizvoda
               String productKey = p.user.Username+":"+p.Name;
               byte[][] key = { Encoding.UTF8.GetBytes("BidPrice") };
               byte[][] values = { Encoding.UTF8.GetBytes(value.ToString ()) };
               client.HMSet(productKey, key, values);

               //lista korisnikovih bidova
               string userBidsKey = Username+":myBids";
               client.RPush(userBidsKey, Encoding.UTF8.GetBytes(p.user.Username + ":" + p.Name + ":" + value.ToString()));
               client.Save();
        }

        public void GetUserProducts (RedisClient client)
        {
            string productstring = Username + ":" + "products";
            byte[][] productslist = client.LRange(productstring, 0, -1);//vraca listu produkata

            int count = productslist.Length;
            for (int i = 0; i<count; i++)
            {
                string productName = Encoding.UTF8.GetString(productslist[i]);
                products.Add (Product.GetProduct (client,Username,productName));// dodamo produkt u listu produkata
            }

            string bidlist = Username + ":" + "bids";
            byte[][] bids = client.LRange(bidlist, 0, -1);
            count = bids.Length;
            for (int i=0;i<count;i++)
            {
                productBids.Add(new Product(System.Text.Encoding.UTF8.GetString(bids[i])));
            }

        }
        public static User GetUser (RedisClient client, string username)
        {    
            string readUser = client.Get<string>(username);
            if (readUser != null)
            {
                User user = new User();
                JObject jUser = JObject.Parse(readUser);
                user.Name = (string)jUser["name"];
                user.Lastname = (string)jUser["lastname"];
                user.PositiveVotes = (int)jUser["positiveVotes"];
                user.NegativeVotes = (int)jUser["negativeVotes"];
                user.Password = (string)jUser["password"];
                user.Money = (float)jUser["money"];
                user.Username = username;

                return user;
            }
            else
                return null;
        }

        public void SaveUser (RedisClient client)
        {
            JObject jUser = new JObject(new JProperty ("password",Password),new JProperty ("name",Name),new JProperty ("lastname",Lastname),new JProperty("money",Money),new JProperty ("positiveVotes",PositiveVotes),new JProperty("negativeVotes",NegativeVotes));
            client.Set(Username, jUser.ToString());
        }
        public void RateUser(RedisClient client,bool positiveVote)
        {
            if (positiveVote)
            {
                PositiveVotes++;
            }
            else
            {
                NegativeVotes++;
            }
            SaveUser(client);
        }

        public User (String n, String ln,String username, String password,float money)
        {
            Username = username;
            Password = password;
            Name = n;
            Lastname = ln;
            Money = money;
            PositiveVotes = 0;
            NegativeVotes = 0;
            products = new List<Product>();
            productBids = new List<Product>();
        }
        public User ()
        {

        }
        public User (string username)
        {
            Username = username;
        }
    }
}
