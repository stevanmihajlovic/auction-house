﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AuctionSystem
{
    public static class Config
    {

        public const int RedisPort = 6379;

        public static string SingleHost
        {
            get { return "localhost"; }
        }

        public static string SingleHostConnectionString
        {
            get
            {
                return SingleHost + ":" + RedisPort;
            }
        }
    }
}
