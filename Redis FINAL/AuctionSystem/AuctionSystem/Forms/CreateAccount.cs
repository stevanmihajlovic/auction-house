﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ServiceStack.Redis;
using ServiceStack.Text;
using AuctionSystem.Domain_Classes;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;



namespace AuctionSystem.Forms
{
    public partial class CreateAccount : Form
    {
        RedisClient client;

        public CreateAccount(RedisClient c)
        {
            client = c;
            InitializeComponent();
        }

        private void createButton_Click(object sender, EventArgs e)
        {
            if (usernameTB.Text!= "" && passwordTB.Text != "")
            {
                string user = client.Get<string>(usernameTB.Text);
                if (user == null)
                {
                    //JObject jUser = new JObject(new JProperty("password", passwordTB.Text), new JProperty("name", nameTB.Text), new JProperty("lastname", lastnameTB.Text), new JProperty("money", 0), new JProperty("rating", 0));
                    //client.Set<string>(usernameTB.Text, jUser.ToString());
                    //string userList = "users";
                    //client.RPush(userList, Encoding.UTF8.GetBytes(usernameTB.Text));
                    User u = new User(nameTB.Text,lastnameTB.Text,usernameTB.Text, passwordTB.Text, 0);
                    u.SaveUser(client);
                    Home form = new Home(u,client);
                    this.Dispose();
                    form.ShowDialog();
                }
                else
                {
                    MessageBox.Show("Username already used.");
                }
            }
        }
    }
}
