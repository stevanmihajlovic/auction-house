﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using AuctionSystem.Domain_Classes;
using AuctionSystem.Forms;


namespace AuctionSystem.Forms
{
    public partial class ExtendTimeForm : Form
    {
        Product product;

        public ExtendTimeForm(Product p)
        {
            product = p;
            InitializeComponent();

            dateTimePicker1.Format = DateTimePickerFormat.Custom;
            dateTimePicker1.CustomFormat = "MM dd yyyy hh mm ss";  
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (dateTimePicker1.Value.Date< DateTime.Now && dateTimePicker1.Value.TimeOfDay<DateTime.Now.TimeOfDay)
            {
                MessageBox.Show("You have to chose valid date and time.");
            }
            else
            {               
                product.BidEnd = dateTimePicker1.Value.Date;
                MessageBox.Show("Thank you. Product will be available for bidding until "+dateTimePicker1.Value.Date.ToShortDateString ());
                this.Dispose();
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Dispose();
        }
    }
}
