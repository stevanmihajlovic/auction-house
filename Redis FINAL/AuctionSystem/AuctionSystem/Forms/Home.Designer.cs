﻿namespace AuctionSystem
{
    partial class Home
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.descriptionTB = new System.Windows.Forms.RichTextBox();
            this.startPriceTB = new System.Windows.Forms.TextBox();
            this.addButton = new System.Windows.Forms.Button();
            this.nameTB = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.rateProductButton = new System.Windows.Forms.Button();
            this.rateProductsLV = new System.Windows.Forms.ListView();
            this.label13 = new System.Windows.Forms.Label();
            this.searchProductList = new System.Windows.Forms.ListView();
            this.HomeTab = new System.Windows.Forms.TabPage();
            this.label17 = new System.Windows.Forms.Label();
            this.negativeVotesTB = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.positiveVotesTB = new System.Windows.Forms.TextBox();
            this.button4 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.refreshButton = new System.Windows.Forms.Button();
            this.label12 = new System.Windows.Forms.Label();
            this.richTextBox1 = new System.Windows.Forms.RichTextBox();
            this.hratingTB = new System.Windows.Forms.TextBox();
            this.hmoneyTB = new System.Windows.Forms.TextBox();
            this.hlnTB = new System.Windows.Forms.TextBox();
            this.hnameTB = new System.Windows.Forms.TextBox();
            this.bidList = new System.Windows.Forms.ListView();
            this.productList = new System.Windows.Forms.ListView();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.tabPage2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.HomeTab.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.textBox1);
            this.tabPage2.Controls.Add(this.groupBox1);
            this.tabPage2.Controls.Add(this.label18);
            this.tabPage2.Controls.Add(this.label16);
            this.tabPage2.Controls.Add(this.rateProductButton);
            this.tabPage2.Controls.Add(this.rateProductsLV);
            this.tabPage2.Controls.Add(this.label13);
            this.tabPage2.Controls.Add(this.searchProductList);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(583, 430);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Product Management";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(291, 398);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(100, 20);
            this.textBox1.TabIndex = 35;
            this.textBox1.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.dateTimePicker1);
            this.groupBox1.Controls.Add(this.descriptionTB);
            this.groupBox1.Controls.Add(this.startPriceTB);
            this.groupBox1.Controls.Add(this.addButton);
            this.groupBox1.Controls.Add(this.nameTB);
            this.groupBox1.Location = new System.Drawing.Point(291, 18);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(281, 341);
            this.groupBox1.TabIndex = 34;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Add new product:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(6, 26);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(76, 13);
            this.label4.TabIndex = 7;
            this.label4.Text = "Product name:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 142);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(83, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Add description:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 108);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(59, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "Start Price:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 67);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(51, 13);
            this.label3.TabIndex = 5;
            this.label3.Text = "Bid Time:";
            // 
            // dateTimePicker1
            // 
            this.dateTimePicker1.Location = new System.Drawing.Point(88, 67);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.Size = new System.Drawing.Size(181, 20);
            this.dateTimePicker1.TabIndex = 6;
            // 
            // descriptionTB
            // 
            this.descriptionTB.Location = new System.Drawing.Point(88, 142);
            this.descriptionTB.Name = "descriptionTB";
            this.descriptionTB.Size = new System.Drawing.Size(181, 117);
            this.descriptionTB.TabIndex = 2;
            this.descriptionTB.Text = "";
            // 
            // startPriceTB
            // 
            this.startPriceTB.Location = new System.Drawing.Point(88, 105);
            this.startPriceTB.Name = "startPriceTB";
            this.startPriceTB.Size = new System.Drawing.Size(99, 20);
            this.startPriceTB.TabIndex = 3;
            // 
            // addButton
            // 
            this.addButton.Location = new System.Drawing.Point(194, 296);
            this.addButton.Name = "addButton";
            this.addButton.Size = new System.Drawing.Size(75, 23);
            this.addButton.TabIndex = 9;
            this.addButton.Text = "Add Product";
            this.addButton.UseVisualStyleBackColor = true;
            this.addButton.Click += new System.EventHandler(this.addButton_Click);
            // 
            // nameTB
            // 
            this.nameTB.Location = new System.Drawing.Point(88, 26);
            this.nameTB.Name = "nameTB";
            this.nameTB.Size = new System.Drawing.Size(181, 20);
            this.nameTB.TabIndex = 8;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(15, 188);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(230, 13);
            this.label18.TabIndex = 33;
            this.label18.Text = "Double click on item for more details and to bid.";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(18, 218);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(88, 13);
            this.label16.TabIndex = 14;
            this.label16.Text = "Bought products:";
            // 
            // rateProductButton
            // 
            this.rateProductButton.Location = new System.Drawing.Point(151, 398);
            this.rateProductButton.Name = "rateProductButton";
            this.rateProductButton.Size = new System.Drawing.Size(94, 23);
            this.rateProductButton.TabIndex = 13;
            this.rateProductButton.Text = "Rate";
            this.rateProductButton.UseVisualStyleBackColor = true;
            this.rateProductButton.Click += new System.EventHandler(this.rateProductButton_Click);
            // 
            // rateProductsLV
            // 
            this.rateProductsLV.Location = new System.Drawing.Point(18, 234);
            this.rateProductsLV.Name = "rateProductsLV";
            this.rateProductsLV.Size = new System.Drawing.Size(227, 162);
            this.rateProductsLV.TabIndex = 12;
            this.rateProductsLV.UseCompatibleStateImageBehavior = false;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(15, 18);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(110, 13);
            this.label13.TabIndex = 11;
            this.label13.Text = "All available products:";
            // 
            // searchProductList
            // 
            this.searchProductList.Location = new System.Drawing.Point(18, 34);
            this.searchProductList.MultiSelect = false;
            this.searchProductList.Name = "searchProductList";
            this.searchProductList.Size = new System.Drawing.Size(227, 151);
            this.searchProductList.TabIndex = 10;
            this.searchProductList.UseCompatibleStateImageBehavior = false;
            this.searchProductList.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.searchProductList_MouseDoubleClick);
            // 
            // HomeTab
            // 
            this.HomeTab.Controls.Add(this.groupBox2);
            this.HomeTab.Controls.Add(this.label17);
            this.HomeTab.Controls.Add(this.button4);
            this.HomeTab.Controls.Add(this.button2);
            this.HomeTab.Controls.Add(this.button1);
            this.HomeTab.Controls.Add(this.refreshButton);
            this.HomeTab.Controls.Add(this.label12);
            this.HomeTab.Controls.Add(this.richTextBox1);
            this.HomeTab.Controls.Add(this.bidList);
            this.HomeTab.Controls.Add(this.productList);
            this.HomeTab.Controls.Add(this.label10);
            this.HomeTab.Controls.Add(this.label9);
            this.HomeTab.Location = new System.Drawing.Point(4, 22);
            this.HomeTab.Name = "HomeTab";
            this.HomeTab.Padding = new System.Windows.Forms.Padding(3);
            this.HomeTab.Size = new System.Drawing.Size(583, 430);
            this.HomeTab.TabIndex = 0;
            this.HomeTab.Text = "HomeTab";
            this.HomeTab.UseVisualStyleBackColor = true;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(22, 379);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(310, 13);
            this.label17.TabIndex = 32;
            this.label17.Text = "Double click on item for detail view or hover over item for details.";
            // 
            // negativeVotesTB
            // 
            this.negativeVotesTB.Location = new System.Drawing.Point(110, 159);
            this.negativeVotesTB.Name = "negativeVotesTB";
            this.negativeVotesTB.Size = new System.Drawing.Size(100, 20);
            this.negativeVotesTB.TabIndex = 31;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(13, 159);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(80, 13);
            this.label15.TabIndex = 30;
            this.label15.Text = "Negative Votes";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(13, 133);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(72, 13);
            this.label14.TabIndex = 29;
            this.label14.Text = "Postive Votes";
            // 
            // positiveVotesTB
            // 
            this.positiveVotesTB.Location = new System.Drawing.Point(110, 133);
            this.positiveVotesTB.Name = "positiveVotesTB";
            this.positiveVotesTB.Size = new System.Drawing.Size(100, 20);
            this.positiveVotesTB.TabIndex = 28;
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(245, 394);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(128, 23);
            this.button4.TabIndex = 27;
            this.button4.Text = "Check sold products";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click_1);
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(110, 185);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(100, 26);
            this.button3.TabIndex = 26;
            this.button3.Text = "Change account info";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(483, 394);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 25;
            this.button2.Text = "Exit";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(402, 394);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 24;
            this.button1.Text = "Log Out";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // refreshButton
            // 
            this.refreshButton.Location = new System.Drawing.Point(497, 146);
            this.refreshButton.Name = "refreshButton";
            this.refreshButton.Size = new System.Drawing.Size(75, 23);
            this.refreshButton.TabIndex = 23;
            this.refreshButton.Text = "Refresh";
            this.refreshButton.UseVisualStyleBackColor = true;
            this.refreshButton.Click += new System.EventHandler(this.refreshButton_Click);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(376, 264);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(103, 13);
            this.label12.TabIndex = 22;
            this.label12.Text = "Product Description:";
            // 
            // richTextBox1
            // 
            this.richTextBox1.Location = new System.Drawing.Point(379, 280);
            this.richTextBox1.Name = "richTextBox1";
            this.richTextBox1.Size = new System.Drawing.Size(179, 96);
            this.richTextBox1.TabIndex = 21;
            this.richTextBox1.Text = "";
            // 
            // hratingTB
            // 
            this.hratingTB.Location = new System.Drawing.Point(110, 106);
            this.hratingTB.Name = "hratingTB";
            this.hratingTB.Size = new System.Drawing.Size(100, 20);
            this.hratingTB.TabIndex = 18;
            // 
            // hmoneyTB
            // 
            this.hmoneyTB.Location = new System.Drawing.Point(110, 80);
            this.hmoneyTB.Name = "hmoneyTB";
            this.hmoneyTB.Size = new System.Drawing.Size(100, 20);
            this.hmoneyTB.TabIndex = 17;
            // 
            // hlnTB
            // 
            this.hlnTB.Location = new System.Drawing.Point(110, 54);
            this.hlnTB.Name = "hlnTB";
            this.hlnTB.Size = new System.Drawing.Size(100, 20);
            this.hlnTB.TabIndex = 16;
            // 
            // hnameTB
            // 
            this.hnameTB.Location = new System.Drawing.Point(110, 28);
            this.hnameTB.Name = "hnameTB";
            this.hnameTB.Size = new System.Drawing.Size(100, 20);
            this.hnameTB.TabIndex = 15;
            // 
            // bidList
            // 
            this.bidList.Location = new System.Drawing.Point(302, 43);
            this.bidList.Name = "bidList";
            this.bidList.Size = new System.Drawing.Size(270, 97);
            this.bidList.TabIndex = 20;
            this.bidList.UseCompatibleStateImageBehavior = false;
            this.bidList.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.bidList_MouseDoubleClick);
            // 
            // productList
            // 
            this.productList.Location = new System.Drawing.Point(24, 280);
            this.productList.Name = "productList";
            this.productList.Size = new System.Drawing.Size(349, 96);
            this.productList.TabIndex = 19;
            this.productList.UseCompatibleStateImageBehavior = false;
            this.productList.ItemMouseHover += new System.Windows.Forms.ListViewItemMouseHoverEventHandler(this.productList_ItemMouseHover);
            this.productList.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.productList_MouseDoubleClick);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(12, 106);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(38, 13);
            this.label11.TabIndex = 12;
            this.label11.Text = "Rating";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(299, 27);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(46, 13);
            this.label10.TabIndex = 8;
            this.label10.Text = "My bids:";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(22, 264);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(76, 13);
            this.label9.TabIndex = 7;
            this.label9.Text = "Your products:";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(12, 83);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(39, 13);
            this.label8.TabIndex = 6;
            this.label8.Text = "Money";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(12, 54);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(58, 13);
            this.label7.TabIndex = 4;
            this.label7.Text = "Last Name";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(12, 28);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(35, 13);
            this.label6.TabIndex = 2;
            this.label6.Text = "Name";
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.HomeTab);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Location = new System.Drawing.Point(0, 0);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(591, 456);
            this.tabControl1.TabIndex = 0;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.button3);
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Controls.Add(this.negativeVotesTB);
            this.groupBox2.Controls.Add(this.label7);
            this.groupBox2.Controls.Add(this.label15);
            this.groupBox2.Controls.Add(this.label8);
            this.groupBox2.Controls.Add(this.label14);
            this.groupBox2.Controls.Add(this.label11);
            this.groupBox2.Controls.Add(this.positiveVotesTB);
            this.groupBox2.Controls.Add(this.hnameTB);
            this.groupBox2.Controls.Add(this.hlnTB);
            this.groupBox2.Controls.Add(this.hmoneyTB);
            this.groupBox2.Controls.Add(this.hratingTB);
            this.groupBox2.Location = new System.Drawing.Point(24, 27);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(257, 234);
            this.groupBox2.TabIndex = 33;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Account information";
            // 
            // Home
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(590, 455);
            this.Controls.Add(this.tabControl1);
            this.Name = "Home";
            this.Text = "Auctioneer";
            this.Activated += new System.EventHandler(this.Home_Activated);
            this.Load += new System.EventHandler(this.Home_Load);
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.HomeTab.ResumeLayout(false);
            this.HomeTab.PerformLayout();
            this.tabControl1.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.ListView searchProductList;
        private System.Windows.Forms.Button addButton;
        private System.Windows.Forms.TextBox nameTB;
        private System.Windows.Forms.TextBox startPriceTB;
        private System.Windows.Forms.RichTextBox descriptionTB;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.DateTimePicker dateTimePicker1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TabPage HomeTab;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button refreshButton;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.RichTextBox richTextBox1;
        private System.Windows.Forms.TextBox hratingTB;
        private System.Windows.Forms.TextBox hmoneyTB;
        private System.Windows.Forms.TextBox hlnTB;
        private System.Windows.Forms.TextBox hnameTB;
        private System.Windows.Forms.ListView bidList;
        private System.Windows.Forms.ListView productList;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.TextBox negativeVotesTB;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox positiveVotesTB;
        private System.Windows.Forms.Button rateProductButton;
        private System.Windows.Forms.ListView rateProductsLV;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;

    }
}