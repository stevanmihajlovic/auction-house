﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using AuctionSystem.Domain_Classes;
using ServiceStack.Redis;
using ServiceStack.Text;
using AuctionSystem.Forms;


namespace AuctionSystem.Forms
{
   

    public partial class SoldProductForm : Form
    {
        User user;
        RedisClient client;
        List<Product> soldProducts;
        List<string> buyers;
        List<int> indices;

        public SoldProductForm(ref User u, RedisClient c)
        {
            user = u;
            client = c;
            soldProducts = new List<Product>();
            buyers = new List<string>();
            indices = new List<int>();
            InitializeComponent();
        }

        private void SoldProductForm_Load(object sender, EventArgs e)
        {
            soldProductList.View = View.Details;
            soldProductList.GridLines = true;
            soldProductList.FullRowSelect = true;

            soldProductList.Columns.Add("Product name", soldProductList.Width/3);
            soldProductList.Columns.Add("Final price", soldProductList.Width / 3);
            soldProductList.Columns.Add("Sold to", soldProductList.Width / 3);
            
            string productsKey = user.Username+":products";
            byte[][] allProducts = client.LRange(productsKey, 0, -1);
            int count = allProducts.Length;

            for (int i=0; i<count; i++)
            {
                Product p = Product.GetProduct(client, user.Username, Encoding.UTF8.GetString(allProducts[i]));
                

                string bidListKey = user.Username + ":" + p.Name + ":bids";
                byte[] lastBid = client.LIndex (bidListKey,-1);
               
                if (p.BidEnd < DateTime.Now)
                {
                    string[] items = new string[3];
                    if (lastBid == null)
                    {
                        items[0] = p.Name;
                        items[1] = "/";
                        items[2] = "None";

                    }
                    else
                    {
                        string lastB = Encoding.UTF8.GetString(lastBid);           
                        string[] splited = lastB.Split(':');
                        items[0] = p.Name;
                        items[1] = splited[1];
                        items[2] = splited[0];

                        soldProducts.Add(p);
                        buyers.Add(splited[0]);
                    }

                    ListViewItem item = new ListViewItem(items);
                    soldProductList.Items.Add(item);
                }
            }

        }

        private void button1_Click(object sender, EventArgs e)
        {
            float totalPrice = 0;
            int index = 0;
            foreach (Product p in soldProducts)
            {
                totalPrice += p.BidPrise;
                p.FinishBidding(client, buyers[index++]);
                soldProductList.Items.RemoveByKey(p.Name);
                user.products.RemoveAll(x => x.Name == p.Name);
            }

            user.Money += totalPrice;
            int count = soldProducts.Count;
            MessageBox.Show("You sold in total "+count.ToString()+" products and earnd "+totalPrice.ToString() +" .Money will be added to you account. Products without any bids will be kept untill you either remove them or extend their bidding time.");
            user.SaveUser(client);
            this.Dispose();
        }
    }
}
