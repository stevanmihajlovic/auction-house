﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Forms;
using AuctionSystem.Domain_Classes;
using ServiceStack.Redis;
using ServiceStack.Text;
using AuctionSystem.Forms;

namespace AuctionSystem
{
    public partial class BidForm : Form
    {
        Product product;
        RedisClient client;
        User user;
        bool localUser;

        public BidForm(Product p, RedisClient c, User u,bool lu)
        {
            product = p;
            client = c;
            user = u;
            localUser = lu;

            InitializeComponent();
            LoadBids();
        }

        private void BidForm_Load(object sender, EventArgs e)
        {
            productNameTB.Text = product.Name;
            ownerTB.Text = product.user.Username;
            priceTB.Text = product.StartPrise.ToString();
            currentPriceTB.Text = product.BidPrise.ToString();
            dateTB.Text = product.BidEnd.ToString();
            descriptionTB.Text = product.Description;

           
            bidList.Columns.Add("Hightes bidder",100);
            bidList.Columns.Add("Bid price",100);

            bidList.View = View.Details;
            bidList.GridLines = true;
            bidList.FullRowSelect = true;

            if (localUser)
            {
                button1.Visible = false;
                bidPriceTB.Visible = false;
                label8.Visible = false;
            }
            
        }

        private void button1_Click(object sender, EventArgs e)
        {
            float price;
            bool success = Single.TryParse(bidPriceTB.Text, out price);
            
            if (success)
            {
                if (product.BidEnd>DateTime.Now)
                {
                    if (user.Money >= price)
                    {
                        if (price > product.BidPrise)
                        {

                            user.BidProduct(product, client, price);
                            bidList.Items.Add(new ListViewItem(new string[] { user.Username, price.ToString() }));//doda odmah u listu bidova novi bez ada cita bazu **** (TMR0)
                            MessageBox.Show("Thanks for bidding.");
                            currentPriceTB.Text = price.ToString();
                        }
                        else { MessageBox.Show("Insert correct bid price"); }
                    }
                    else
                    {
                        MessageBox.Show("You don't have enough money for bid.");
                    }
                }
                else
                {
                    MessageBox.Show("Bidding time ended.");
                }
            }
            else
            {
                MessageBox.Show("Insert correct value.");
            }

        }

        private void LoadBids ()
        {
            
            string bidKey = product.user.Username + ":" + product.Name +":"+"bids";
            byte[][] bids = client.LRange (bidKey,0,-1);

            int length = bids.Length;
            for (int i=0;i<length;i++)
            {
                string bid = Encoding.UTF8.GetString(bids[i]);
                bidList.Items.Add(new ListViewItem (bid.Split (':')));
            }

        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (bidList.Items.Count == 0 && product.BidEnd < DateTime.Now)
            {
                if (product.BidEnd < DateTime.Now)
                {
                    product.FinishBidding(client,null);
                    MessageBox.Show("Product successfully removed.");
                    this.Dispose();
                }
                else
                {
                    MessageBox.Show("You can not romove product until bidding is over.");
                }
            }
            else
            {
                MessageBox.Show("You can not remove product that product.");
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (bidList.Items.Count == 0)
            {
                if (product.BidEnd < DateTime.Now)
                {
                    ExtendTimeForm form = new ExtendTimeForm(product);
                    form.ShowDialog();
                    this.Activate();
                }
                else
                {
                    MessageBox.Show("While product is available for bidding, you can not change bid end time");
                }
            }
            else
            {
                MessageBox.Show("You can not change bidding end time, bacuse product has active bids");
            }
        }

        private void BidForm_Activated(object sender, EventArgs e)
        {
            dateTB.Text = product.BidEnd.ToString();
        }
    }
}
