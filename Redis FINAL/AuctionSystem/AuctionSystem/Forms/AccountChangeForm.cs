﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using AuctionSystem.Domain_Classes;
using ServiceStack.Redis;
using ServiceStack.Text;
using AuctionSystem.Forms;


namespace AuctionSystem.Forms
{
    public partial class AccountChangeForm : Form
    {
        User user;
        RedisClient client;

        public AccountChangeForm(User user,RedisClient client)
        {
            this.user = user;
            this.client = client;

            InitializeComponent();
        }

        private void AccountChangeForm_Load(object sender, EventArgs e)
        {
            usernameTB.Text = user.Username;
            firstnameTB.Text = user.Name;
            lastnameTB.Text = user.Lastname;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                bool accountChanged = false;

                if (firstnameTB.Text != user.Name)
                {
                    accountChanged = true;
                    user.Name = firstnameTB.Text;
                }
                if (lastnameTB.Text != user.Lastname)
                {
                    accountChanged = true;
                    user.Lastname = lastnameTB.Text;
                }
                if (moneyTB.Text != "")
                {
                    float amount;
                    if (Single.TryParse(moneyTB.Text, out amount))
                    {
                        user.Money += amount;
                        accountChanged = true;
                    }
                    else
                    {
                        moneyTB.Text = "";
                        throw new Exception("Wrong money value!");
                        //MessageBox.Show("Wrong money value!");
                    }
                }
                if (oldpasswordTB.Text != "" && newpasswordTB.Text != "" && conpasswordTB.Text != "")
                {
                    if (oldpasswordTB.Text == user.Password && newpasswordTB.Text == conpasswordTB.Text)
                    {
                        user.Password = newpasswordTB.Text;
                        accountChanged = true;
                    }
                    else
                    {
                        throw new Exception("Incorrect password. Please retype you password.");
                        //MessageBox.Show("Incorrect password. Please retype you password.");
                    }
                }
                if (accountChanged)
                    user.SaveUser(client);
                this.Close();
            }
            catch (Exception err)
            {
                MessageBox.Show (err.Message);
            }
        }
    }
}
