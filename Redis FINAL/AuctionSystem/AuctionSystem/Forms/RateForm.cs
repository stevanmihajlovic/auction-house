﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Forms;
using AuctionSystem.Domain_Classes;
using ServiceStack.Redis;
using ServiceStack.Text;
using AuctionSystem.Forms;

namespace AuctionSystem.Forms
{
    public partial class RateForm : Form
    {
        string productName;
        RedisClient client;
        User user;

        public RateForm(User u,string pname, RedisClient c)
        {
            
            productName = pname;
            client = c;
            user = u;
            InitializeComponent();
        }


        private void button1_Click(object sender, EventArgs e)
        {
            bool positiveVote;
            if (radioButton1.Checked)
                positiveVote = true;
            else
                positiveVote = false;
            user.RateUser(client, positiveVote);
            MessageBox.Show("Thank you.");
            this.Dispose();
        }

        private void RateForm_Load(object sender, EventArgs e)
        {
            pNameLabel.Text = productName;
            ownerLabel.Text = user.Username;
        }
    }
}
