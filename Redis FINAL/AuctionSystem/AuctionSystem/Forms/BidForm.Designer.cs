﻿namespace AuctionSystem
{
    partial class BidForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.productNameTB = new System.Windows.Forms.TextBox();
            this.ownerTB = new System.Windows.Forms.TextBox();
            this.priceTB = new System.Windows.Forms.TextBox();
            this.currentPriceTB = new System.Windows.Forms.TextBox();
            this.dateTB = new System.Windows.Forms.TextBox();
            this.descriptionTB = new System.Windows.Forms.RichTextBox();
            this.bidList = new System.Windows.Forms.ListView();
            this.button1 = new System.Windows.Forms.Button();
            this.bidPriceTB = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.button2 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 23);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(76, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Product name:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 55);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(41, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Owner:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 89);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(71, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Original price:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(12, 127);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(70, 13);
            this.label4.TabIndex = 3;
            this.label4.Text = "Current price:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(12, 165);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(83, 13);
            this.label5.TabIndex = 4;
            this.label5.Text = "Bidding ends at:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(12, 205);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(63, 13);
            this.label6.TabIndex = 5;
            this.label6.Text = "Description:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(270, 7);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(67, 13);
            this.label7.TabIndex = 6;
            this.label7.Text = "Current Bids:";
            // 
            // productNameTB
            // 
            this.productNameTB.Location = new System.Drawing.Point(104, 23);
            this.productNameTB.Name = "productNameTB";
            this.productNameTB.Size = new System.Drawing.Size(100, 20);
            this.productNameTB.TabIndex = 7;
            // 
            // ownerTB
            // 
            this.ownerTB.Location = new System.Drawing.Point(104, 55);
            this.ownerTB.Name = "ownerTB";
            this.ownerTB.Size = new System.Drawing.Size(100, 20);
            this.ownerTB.TabIndex = 8;
            // 
            // priceTB
            // 
            this.priceTB.Location = new System.Drawing.Point(104, 89);
            this.priceTB.Name = "priceTB";
            this.priceTB.Size = new System.Drawing.Size(100, 20);
            this.priceTB.TabIndex = 9;
            // 
            // currentPriceTB
            // 
            this.currentPriceTB.Location = new System.Drawing.Point(104, 127);
            this.currentPriceTB.Name = "currentPriceTB";
            this.currentPriceTB.Size = new System.Drawing.Size(100, 20);
            this.currentPriceTB.TabIndex = 10;
            // 
            // dateTB
            // 
            this.dateTB.Location = new System.Drawing.Point(104, 162);
            this.dateTB.Name = "dateTB";
            this.dateTB.Size = new System.Drawing.Size(100, 20);
            this.dateTB.TabIndex = 11;
            // 
            // descriptionTB
            // 
            this.descriptionTB.Location = new System.Drawing.Point(104, 205);
            this.descriptionTB.Name = "descriptionTB";
            this.descriptionTB.Size = new System.Drawing.Size(161, 70);
            this.descriptionTB.TabIndex = 12;
            this.descriptionTB.Text = "";
            // 
            // bidList
            // 
            this.bidList.Location = new System.Drawing.Point(273, 23);
            this.bidList.Name = "bidList";
            this.bidList.Size = new System.Drawing.Size(191, 111);
            this.bidList.TabIndex = 13;
            this.bidList.UseCompatibleStateImageBehavior = false;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(406, 140);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(58, 23);
            this.button1.TabIndex = 14;
            this.button1.Text = "Bid";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // bidPriceTB
            // 
            this.bidPriceTB.Location = new System.Drawing.Point(326, 142);
            this.bidPriceTB.Name = "bidPriceTB";
            this.bidPriceTB.Size = new System.Drawing.Size(64, 20);
            this.bidPriceTB.TabIndex = 15;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(270, 145);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(50, 13);
            this.label8.TabIndex = 16;
            this.label8.Text = "Bid prise:";
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(273, 251);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(102, 23);
            this.button2.TabIndex = 17;
            this.button2.Text = "Remove Product";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(381, 251);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(91, 23);
            this.button3.TabIndex = 18;
            this.button3.Text = "Extend Time";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // BidForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(485, 300);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.bidPriceTB);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.bidList);
            this.Controls.Add(this.descriptionTB);
            this.Controls.Add(this.dateTB);
            this.Controls.Add(this.currentPriceTB);
            this.Controls.Add(this.priceTB);
            this.Controls.Add(this.ownerTB);
            this.Controls.Add(this.productNameTB);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "BidForm";
            this.Text = "BidForm";
            this.Activated += new System.EventHandler(this.BidForm_Activated);
            this.Load += new System.EventHandler(this.BidForm_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox productNameTB;
        private System.Windows.Forms.TextBox ownerTB;
        private System.Windows.Forms.TextBox priceTB;
        private System.Windows.Forms.TextBox currentPriceTB;
        private System.Windows.Forms.TextBox dateTB;
        private System.Windows.Forms.RichTextBox descriptionTB;
        private System.Windows.Forms.ListView bidList;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox bidPriceTB;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button3;
    }
}