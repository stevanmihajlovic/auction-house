﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using AuctionSystem.Domain_Classes;
using ServiceStack.Redis;
using ServiceStack.Text;
using AuctionSystem.Forms;

namespace AuctionSystem
{
    public partial class Home : Form
    {
        User user;
        RedisClient client;

        public Home(User u,RedisClient  c)
        {
            user = u;
            client = c;
            InitializeComponent();



            //foreach (Product p in u.productBids)
            //{
            //    bidList.Items.Add(p.Name);
            //}

            
        }

        private void addButton_Click(object sender, EventArgs e)
        {
            if (nameTB.Text != "" && startPriceTB.Text != "" && descriptionTB.Text != "" && dateTimePicker1.Value.Date > DateTime.Now)
            {
                bool parse;
                float price;
                parse = Single.TryParse(startPriceTB.Text, out price);
                if (parse)
                {
                    Product p = new Product(nameTB.Text, descriptionTB.Text, price, dateTimePicker1.Value.Date);
                    if (!user.AddProduct(p, client))
                        MessageBox.Show("You already have product with that name. Plesae rename product.");
                    else
                    {
                        tabControl1.SelectTab("HomeTab");
                        UpdateProductList(p);
                    }
                }
                else 
                {
                    MessageBox.Show("Please insert correct price format");
                }
            }
            else
            {
                MessageBox.Show("Wrong product properties.");
            }
        }

        private void Home_Load(object sender, EventArgs e)
        {
            productList.View = View.Details;
            productList.GridLines = true;
            productList.FullRowSelect = true;

            productList.Columns.Add("Product name",productList.Width/4);
            productList.Columns.Add("Start Price", productList.Width / 4);
            productList.Columns.Add("Current bid", productList.Width / 4);
            productList.Columns.Add("Expiration Date", productList.Width / 4);

            LoadUserProducts();

            searchProductList.View = View.Details;
            searchProductList.GridLines = true;
            searchProductList.FullRowSelect = true;

            searchProductList.Columns.Add("Owner", searchProductList.Width/2);
            searchProductList.Columns.Add("Product name", searchProductList.Width / 2);

            string allproducts = "allProducts";
            byte[][] products = client.LRange(allproducts, 0, -1);
            int count = products.Length;

            for (int i = 0; i<count; i++)
            {
                ListViewItem item;
                string product = Encoding.UTF8.GetString(products[i]);
                string[] splited = product.Split('.');
                item = new ListViewItem(splited);
                searchProductList.Items.Add(item);        
            }

            bidList.View = View.Details;
            bidList.GridLines = true;
            bidList.FullRowSelect = true;
            bidList.Columns.Add("Product owner",bidList.Width/3);
            bidList.Columns.Add("Product name", bidList.Width / 3);
            bidList.Columns.Add("Your bid", bidList.Width / 3);

            //pribavljanje nasih bidiva
            string bidsKey = user.Username + ":myBids";
            byte[][] bids = client.LRange(bidsKey, 0, -1);
            count = bids.Length;
            for (int i = 0; i<count;i++)
            {
                string bid = Encoding.UTF8.GetString(bids[i]);
                bidList.Items.Add(new ListViewItem(bid.Split(':')));
            }

            rateProductsLV.View = View.Details;
            rateProductsLV.GridLines = true;
            rateProductsLV.FullRowSelect = true;

            rateProductsLV.Columns.Add("Product name", rateProductsLV.Width/2);
            rateProductsLV.Columns.Add("Bought from", rateProductsLV.Width / 2);

            string boughtProductsKey = user.Username + ":boughtProducts";
            byte[][] boughtP = client.LRange(boughtProductsKey,0, -1);
            count = boughtP.Length;
            for (int i=0; i<count;i++)
            {
                string p = Encoding.UTF8.GetString(boughtP[i]);
                rateProductsLV.Items.Add(new ListViewItem(p.Split(':')));
            }

        }

        private void productList_ItemMouseHover(object sender, ListViewItemMouseHoverEventArgs e)
        {
            int index = e.Item.Index;
            richTextBox1.Text = user.products[index].Description;
        }
        public void UpdateProductList (Product p)
        {
            ListViewItem item;
            string[] array = new string[4];
            array[0] = p.Name;
            array[1] = p.StartPrise.ToString();
            array[2] = p.BidPrise.ToString();
            array[3] = p.BidEnd.ToString();

            item = new ListViewItem(array);
            productList.Items.Add(item);
        }

        private void searchProductList_DoubleClick(object sender, EventArgs e)
        {
            ListViewItem item = searchProductList.SelectedItems[0];
            string productName = item.SubItems[1].Text;
            string owner = item.SubItems[0].Text;
            Product p = Product.GetProduct(client, owner, productName);

            bool localUser = p.user.Username.Equals(user.Username) ? true : false;

            BidForm form = new BidForm(p, client, user,localUser);
            form.ShowDialog();

        }

        private void refreshButton_Click(object sender, EventArgs e)
        {
            bidList.Items.Clear();
            string bidsKey = user.Username + ":myBids";
            byte[][] bids = client.LRange(bidsKey, 0, -1);
            int count = bids.Length;
            for (int i = 0; i < count; i++)
            {
                string bid = Encoding.UTF8.GetString(bids[i]);
                bidList.Items.Add(new ListViewItem(bid.Split(':')));
            }
        }

        private void productList_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            ListViewItem item = productList.SelectedItems[0];
            Product p = Product.GetProduct(client, user.Username, item.SubItems[0].Text);
            
            

            BidForm bf = new BidForm(p, client, user,true);
            bf.ShowDialog();

        }

        private void bidList_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            ListViewItem item = bidList.SelectedItems[0];
            Product p = Product.GetProduct(client, item.SubItems[0].Text, item.SubItems[1].Text);
            BidForm bf = new BidForm(p, client, user, false);
            bf.ShowDialog();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            //Form1 form = new Form1();
            //form.ShowDialog();
            //this.Close();
            Dispose();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            AccountChangeForm form = new AccountChangeForm(user, client);
            this.Hide();
            form.ShowDialog();
            this.Show();
            this.Activate();
            
        }

        private void Home_Activated(object sender, EventArgs e)
        {
            LoadUserProducts();
        }

        

        private void searchProductList_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            ListViewItem item = searchProductList.SelectedItems[0];
            string productName = item.SubItems[1].Text;
            string owner = item.SubItems[0].Text;
            Product p = Product.GetProduct(client, owner, productName);
            if (p != null)
            {
                bool localUser = p.user.Username.Equals(user.Username) ? true : false;
                BidForm form = new BidForm(p, client, user, localUser);
                form.ShowDialog();
            }
            else
            {
                MessageBox.Show("User has removed that item.");
                searchProductList.Items.Remove(item);
            }
        }

        private void button4_Click_1(object sender, EventArgs e)
        {
            SoldProductForm form = new SoldProductForm(ref user, client);
            form.ShowDialog();
            LoadUserProducts();

        }
        void LoadUserProducts ()
        {
            productList.Items.Clear();//da se ne dupliraju nakon loada i activate-a
            int productCount = user.products.Count;
            for (int i = 0; i < productCount; i++)
            {
                ListViewItem item;
                string[] array = new string[4];
                array[0] = user.products[i].Name;
                array[1] = user.products[i].StartPrise.ToString();
                array[2] = user.products[i].BidPrise.ToString();
                array[3] = user.products[i].BidEnd.ToString();

                item = new ListViewItem(array);
                productList.Items.Add(item);

            }
                hnameTB.Text = user.Name;
                hlnTB.Text = user.Lastname;
                //hratingTB.Text = ((user.PositiveVotes + user.NegativeVotes)!= 0) ? ((float)(user.PositiveVotes / (user.PositiveVotes + user.NegativeVotes))).ToString() : "User has no votes yet.";
                if (user.PositiveVotes != 0 || user.NegativeVotes != 0)
                {
                    float rat = 5*(float)user.PositiveVotes / ((float)user.PositiveVotes + (float)user.NegativeVotes);
                    hratingTB.Text = rat.ToString();
                }
                else
                    hratingTB.Text = "User has no votes yet.";
                hmoneyTB.Text = user.Money.ToString();
                positiveVotesTB.Text = user.PositiveVotes.ToString();
                negativeVotesTB.Text = user.NegativeVotes.ToString();    
        }

        private void rateProductButton_Click(object sender, EventArgs e)
        {
            if (rateProductsLV.SelectedItems.Count != 0)
            {
                User owner = User.GetUser(client, rateProductsLV.SelectedItems[0].SubItems[0].Text);
                RateForm form = new RateForm(owner, rateProductsLV.SelectedItems[0].SubItems[1].Text, client);
                form.ShowDialog();
                rateProductsLV.Items.Remove(rateProductsLV.SelectedItems[0]);
            }
            else
            {
                MessageBox.Show("Please select item first");
            }
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            string text = textBox1.Text;
            foreach (ListViewItem item in searchProductList.Items)
            {
                if (!item.SubItems[0].Text.Contains(text))
                    searchProductList.Items.Remove(item);
            }
        }
    }
}
