﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ServiceStack.Redis;
using ServiceStack.Text;
using AuctionSystem.Domain_Classes;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using AuctionSystem.Forms;


namespace AuctionSystem
{
    public partial class Form1 : Form
    {
        User User;
        RedisClient client;

        public Form1()
        {
            CreateRedisClient();
            InitializeComponent();
        }
        public void CreateRedisClient ()
        {
            if (client == null)
                client = new RedisClient (Config.SingleHost);
        }

        private void loginButton_Click(object sender, EventArgs e)
        {
            if (usernameTB.Text != "" && passwordTB.Text !="")
            {
                string user = client.Get<string>(usernameTB.Text);
                if (user != null)
                {
                    JObject jo = JObject.Parse(user);
                    string pass = (string)jo["password"];
                    string name = (string)jo["name"];
                    string lastname = (string)jo["lastname"];
                    int posVotes = (int)jo["positiveVotes"];
                    int negVotes = (int)jo["negativeVotes"];

                    if(pass == passwordTB.Text)
                    {
                        float r,m;
                        //r= (float)jo["rating"];
                        m= (float)jo["money"];
                        User = new User(name,lastname,usernameTB.Text,pass,m);
                        User.GetUserProducts(client);
                        User.PositiveVotes = posVotes;
                        User.NegativeVotes = negVotes;

                        Home form = new Home(User,client);
                        form.ShowDialog();
                        //this.Dispose();
                        
                    }
                    else
                    {
                        MessageBox.Show("Wrong username or password.");
                    }
                }
                else
                {
                    MessageBox.Show("Wrong username or password.");
                }
            }
        }

        private void createButton_Click(object sender, EventArgs e)
        {
            CreateAccount form = new CreateAccount(client);
            form.ShowDialog();
            this.Dispose();
            
        }
    }
}
