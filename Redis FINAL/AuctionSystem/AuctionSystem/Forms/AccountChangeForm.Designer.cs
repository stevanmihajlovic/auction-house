﻿namespace AuctionSystem.Forms
{
    partial class AccountChangeForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.usernameTB = new System.Windows.Forms.TextBox();
            this.firstnameTB = new System.Windows.Forms.TextBox();
            this.lastnameTB = new System.Windows.Forms.TextBox();
            this.oldpasswordTB = new System.Windows.Forms.TextBox();
            this.newpasswordTB = new System.Windows.Forms.TextBox();
            this.conpasswordTB = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.moneyTB = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(58, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Username:";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(12, 60);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(61, 13);
            this.label8.TabIndex = 1;
            this.label8.Text = "Last Name:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 36);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(60, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "First Name:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(246, 60);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(93, 13);
            this.label3.TabIndex = 3;
            this.label3.Text = "Confirm password:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(246, 36);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(80, 13);
            this.label4.TabIndex = 4;
            this.label4.Text = "New password:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(246, 13);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(75, 13);
            this.label5.TabIndex = 5;
            this.label5.Text = "Old Password:";
            // 
            // usernameTB
            // 
            this.usernameTB.Location = new System.Drawing.Point(89, 13);
            this.usernameTB.Name = "usernameTB";
            this.usernameTB.ReadOnly = true;
            this.usernameTB.Size = new System.Drawing.Size(151, 20);
            this.usernameTB.TabIndex = 6;
            // 
            // firstnameTB
            // 
            this.firstnameTB.Location = new System.Drawing.Point(89, 36);
            this.firstnameTB.Name = "firstnameTB";
            this.firstnameTB.Size = new System.Drawing.Size(151, 20);
            this.firstnameTB.TabIndex = 7;
            // 
            // lastnameTB
            // 
            this.lastnameTB.Location = new System.Drawing.Point(89, 60);
            this.lastnameTB.Name = "lastnameTB";
            this.lastnameTB.Size = new System.Drawing.Size(151, 20);
            this.lastnameTB.TabIndex = 8;
            // 
            // oldpasswordTB
            // 
            this.oldpasswordTB.Location = new System.Drawing.Point(347, 13);
            this.oldpasswordTB.Name = "oldpasswordTB";
            this.oldpasswordTB.Size = new System.Drawing.Size(162, 20);
            this.oldpasswordTB.TabIndex = 9;
            // 
            // newpasswordTB
            // 
            this.newpasswordTB.Location = new System.Drawing.Point(347, 39);
            this.newpasswordTB.Name = "newpasswordTB";
            this.newpasswordTB.Size = new System.Drawing.Size(162, 20);
            this.newpasswordTB.TabIndex = 10;
            // 
            // conpasswordTB
            // 
            this.conpasswordTB.Location = new System.Drawing.Point(347, 65);
            this.conpasswordTB.Name = "conpasswordTB";
            this.conpasswordTB.Size = new System.Drawing.Size(162, 20);
            this.conpasswordTB.TabIndex = 11;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(13, 83);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(64, 13);
            this.label6.TabIndex = 12;
            this.label6.Text = "Add Money:";
            // 
            // moneyTB
            // 
            this.moneyTB.Location = new System.Drawing.Point(89, 86);
            this.moneyTB.Name = "moneyTB";
            this.moneyTB.Size = new System.Drawing.Size(151, 20);
            this.moneyTB.TabIndex = 13;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(347, 91);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(162, 23);
            this.button1.TabIndex = 14;
            this.button1.Text = "Change Account";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // AccountChangeForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(529, 123);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.moneyTB);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.conpasswordTB);
            this.Controls.Add(this.newpasswordTB);
            this.Controls.Add(this.oldpasswordTB);
            this.Controls.Add(this.lastnameTB);
            this.Controls.Add(this.firstnameTB);
            this.Controls.Add(this.usernameTB);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label1);
            this.Name = "AccountChangeForm";
            this.Text = "AccountChangeForm";
            this.Load += new System.EventHandler(this.AccountChangeForm_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox usernameTB;
        private System.Windows.Forms.TextBox firstnameTB;
        private System.Windows.Forms.TextBox lastnameTB;
        private System.Windows.Forms.TextBox oldpasswordTB;
        private System.Windows.Forms.TextBox newpasswordTB;
        private System.Windows.Forms.TextBox conpasswordTB;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox moneyTB;
        private System.Windows.Forms.Button button1;
    }
}